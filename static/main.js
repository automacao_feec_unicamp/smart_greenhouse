//                            SMART GREENHOUSE                             //
//                                                                         //
// UNIVERSITY OF CAMPINAS (UNICAMP)                                        //
// SCHOOL OF ELECTRICAL ENGINEERING AND COMPUTING                          //
// IA351D - AUTOMATION PROJECT WITH RASPBERRY PI - 2016.2                  //
// AUTORS: FELICIO HARLEY GARCIA DE CASTRO and JUAN PABLO PANTOJA BASTIDAS //
// TEACHERS: FABIAO FRUETT and LUCAS HEITZMANN GABRIELLI                   //

// VARIABLES DEFINITIONS
var preset_program_status
var door_status;
var heater_lamp_status;
var heater_fan_status;
var cooler_fan_status;
var circulation_fan_status;
var pulverizer_status;
var irrigator_status;
var red_status;
var green_status;
var blue_status;
var color_status
var actuators;

// CREATING A SOCKET INSTANCE
var socket = new io("http://" + document.domain + ":" + location.port);
socket.on('connect', function(){console.log("connected");});
socket.on('disconnect', function(){console.log("disconnected");});

// RECEIVES THE MESSAGES FROM THE SERVER AND PRINTS RECEIVED DATA ON THE CONSOLE
socket.on('message', function(msg){
	console.log("received: "+msg)
	var sensor = msg.split(",");
	//document.getElementById("raspi_temperature_status").innerHTML = sensor[0];
	document.getElementById("amb_temperature_status").innerHTML = sensor[1];
	document.getElementById("amb_humidity_status").innerHTML = sensor[2];
	document.getElementById("soil_humidity_status").innerHTML = sensor[3];
    document.getElementById("water_level_status").innerHTML = sensor[4];
	
	preset_program_status = sensor[5];
		if(preset_program_status==1){
		document.getElementById("preset_program_status").innerHTML = "1"
		}
		else if(preset_program_status==2){
		document.getElementById("preset_program_status").innerHTML = "2"
		}
		else if(preset_program_status==3){
		document.getElementById("preset_program_status").innerHTML = "3"
		}
		else if(preset_program_status==4){
		document.getElementById("preset_program_status").innerHTML = "Particular"
		}
		else{
		document.getElementById("preset_program_status").innerHTML = "-"
		}

	door_status = sensor[6];
		if(door_status==1){
		document.getElementById("door_status").innerHTML = "Open"
		}
		else{
		document.getElementById("door_status").innerHTML = "Close"
		}
	
	heater_lamp_status = sensor[7];
		
		if(heater_lamp_status==1){
		document.getElementById("heater_lamp_status").innerHTML= "On"
		}
		else{
		document.getElementById("heater_lamp_status").innerHTML= "Off"
		}

	heater_fan_status = sensor[8];

		if(heater_fan_status==1){
		document.getElementById("heater_fan_status").innerHTML = "On"		
		}
		else{
		document.getElementById("heater_fan_status").innerHTML = "Off"	
		}

	cooler_fan_status = sensor[9];

		if(cooler_fan_status==1){
		document.getElementById("cooler_fan_status").innerHTML = "On"		
		}
		else{
		document.getElementById("cooler_fan_status").innerHTML = "Off"	
		}

	circulation_fan_status = sensor[10];

		if(circulation_fan_status==1){
		document.getElementById("circulation_fan_status").innerHTML = "On"		
		}
		else{
		document.getElementById("circulation_fan_status").innerHTML = "Off"	
		}

	pulverizer_status = sensor[11];

		if(pulverizer_status==1){
		document.getElementById("pulverizer_status").innerHTML = "On"		
		}
		else{
		document.getElementById("pulverizer_status").innerHTML = "Off"	
		}

	irrigator_status = sensor[12];

		if(irrigator_status==1){
		document.getElementById("irrigator_status").innerHTML = "On"		
		}
		else{
		document.getElementById("irrigator_status").innerHTML = "Off"	
		}

	red_status = sensor[13];
	green_status = sensor[14];
	blue_status = sensor[15];

		if (blue_status != 0 || red_status != 0 || green_status != 0){
		document.getElementById("light_status").innerHTML = "On"
		}
		else{
		document.getElementById("light_status").innerHTML = "Off"
		}
	color_status = sensor[16]
	document.getElementById("color_status").setAttribute("style", "background-color: " + color_status);


    //document.getElementById("datetime").innerHTML = (new Date(Date.now())).toLocaleTimeString();
});

// FUNCTIONS DEFINITIONS

function update_actuators() {
	// Updates the actuators vector using data received from the server
	
	/* actuators
	data0 = preset_program_actuate
	data1 = door_actuate
	data2 = heater_lamp_actuate
	data3 = heater_fan_actuate
	data4 = cooler_fan_actuate
	data5 = circulation_fan_actuate
	data6 = pulverizer_actuate
	data7 = irrigator_actuate
	data8 = red_led_actuate
	data9 = green_led_actuate
	data10 = blue_led_actuate
*/
	actuators = [preset_program_status,door_status,heater_lamp_status,heater_fan_status,cooler_fan_status,circulation_fan_status,pulverizer_status,irrigator_status,red_status,green_status,blue_status];

}

function turn_light_off(actuators) {
	// Command to turn the RGB LED strip off
	actuators[8] = "0";
	actuators[9] = "0";
	actuators[10] = "0";
    return actuators;
}

function send_string(string) {
	// Sends data to the server and prints the sent data on the console
	socket.send(string);
	console.log("sent: "+string)
}

// SENDING COMMANDS TO THE SERVER
var profile1 = document.getElementById("profile1");
profile1.addEventListener("click", function(){
	update_actuators();
	actuators[0] = "1";
	actuators_string = actuators.join(); 
	console.log("PROFILE 1");
	send_string(actuators_string);
});

var profile2 = document.getElementById("profile2");
profile2.addEventListener("click", function(){
	update_actuators();
	actuators[0] = "2";
	actuators_string = actuators.join(); 
	console.log("PROFILE 1");
	send_string(actuators_string);
});

var profile3 = document.getElementById("profile3");
profile3.addEventListener("click", function(){
	update_actuators();
	actuators[0] = "3";
	actuators_string = actuators.join(); 
	console.log("PROFILE 1");
	send_string(actuators_string);
});

var particular = document.getElementById("particular");
particular.addEventListener("click", function(){
	update_actuators();
	actuators[0] = "4";
	actuators_string = actuators.join(); 
	console.log("PROFILE 1");
	send_string(actuators_string);
});

var dooron = document.getElementById("doorOn");
dooron.addEventListener("click", function(){
	update_actuators();
    actuators[0] = "0";
	actuators[1] = "1";
	actuators_string = actuators.join(); 
	console.log("DOOR OPEN");
	send_string(actuators_string);
});

var dooroff = document.getElementById("doorOff");
dooroff.addEventListener("click", function(){
	update_actuators();
    actuators[0] = "0";
	actuators[1] = "0";
	actuators_string = actuators.join(); 
	console.log("DOOR CLOSE");
	send_string(actuators_string);
});

var heaterLampOn = document.getElementById("heaterLampOn");
heaterLampOn.addEventListener("click", function(){
	update_actuators();
    actuators[0] = "0";
	actuators[2] = "1";
	actuators_string = actuators.join(); 
	console.log("HEATER LAMP ON");
	send_string(actuators_string);
});

var heaterLampOff = document.getElementById("heaterLampOff");
heaterLampOff.addEventListener("click", function(){
	update_actuators();
    actuators[0] = "0";
	actuators[2] = "0";
	actuators_string = actuators.join(); 
	console.log("HEATER LAMP OFF");
	send_string(actuators_string);
});

var heaterFanOn = document.getElementById("heaterFanOn");
heaterFanOn.addEventListener("click", function(){
	update_actuators();
    actuators[0] = "0";
	actuators[3] = "1";
	actuators_string = actuators.join(); 
	console.log("HEATER FAN ON");
	send_string(actuators_string);
});

var heaterFanOff = document.getElementById("heaterFanOff");
heaterFanOff.addEventListener("click", function(){
	update_actuators();
    actuators[0] = "0";
	actuators[3] = "0";
	actuators_string = actuators.join(); 
	console.log("HEATER FAN OFF");
	send_string(actuators_string);
});

var coolerFanOn = document.getElementById("coolerFanOn");
coolerFanOn.addEventListener("click", function(){
	update_actuators();
    actuators[0] = "0";
	actuators[4] = "1";
	actuators_string = actuators.join(); 
	console.log("COOLER FAN ON");
	send_string(actuators_string);
});

var coolerFanOff = document.getElementById("coolerFanOff");
coolerFanOff.addEventListener("click", function(){
	update_actuators();
    actuators[0] = "0";
	actuators[4] = "0";
	actuators_string = actuators.join(); 
	console.log("COOLER FAN OFF");
	send_string(actuators_string);
});

var circFanOn = document.getElementById("circFanOn");
circFanOn.addEventListener("click", function(){
	update_actuators();
    actuators[0] = "0";
	actuators[5] = "1";
	actuators_string = actuators.join(); 
	console.log("CIRCULATION FAN ON");
	send_string(actuators_string);
});

var circFanOff = document.getElementById("circFanOff");
circFanOff.addEventListener("click", function(){
	update_actuators();
    actuators[0] = "0";
	actuators[5] = "0";
	actuators_string = actuators.join(); 
	console.log("CIRCULATION FAN OFF");
	send_string(actuators_string);
});

var pulverizerOn = document.getElementById("pulverizerOn");
pulverizerOn.addEventListener("click", function(){
	update_actuators();
    actuators[0] = "0";
	actuators[6] = "1";
	actuators_string = actuators.join(); 
	console.log("PULVERIZER ON");
	send_string(actuators_string);
});

var pulverizerOff = document.getElementById("pulverizerOff");
pulverizerOff.addEventListener("click", function(){
	update_actuators();
    actuators[0] = "0";
	actuators[6] = "0";
	actuators_string = actuators.join(); 
	console.log("PULVERIZER OFF");
	send_string(actuators_string);
});

var irrigatorOn = document.getElementById("irrigatorOn");
irrigatorOn.addEventListener("click", function(){
	update_actuators();
    actuators[0] = "0";
	actuators[7] = "1";
	actuators_string = actuators.join(); 
	console.log("IRRIGATOR ON");
	send_string(actuators_string);
});

var irrigatorOff = document.getElementById("irrigatorOff");
irrigatorOff.addEventListener("click", function(){
	update_actuators();
    actuators[0] = "0";
	actuators[7] = "0";
	actuators_string = actuators.join(); 
	console.log("IRRIGATOR OFF");
	send_string(actuators_string);
});

var lighton = document.getElementById("lightOn");
lighton.addEventListener("click", function(){
	update_actuators();
    actuators[0] = "0";
	actuators[8] = "4095";
	actuators[9] = "4095";
	actuators[10] = "4095";
	actuators_string = actuators.join(); 
	console.log("LIGHT ON");
	send_string(actuators_string);
});

var lightoff = document.getElementById("lightOff");
lightoff.addEventListener("click", function(){
	update_actuators();
    actuators[0] = "0";
	actuators = turn_light_off(actuators);
	actuators_string = actuators.join(); 
	console.log("LIGHT OFF");
	send_string(actuators_string);
});

var redrange = document.getElementById("redRange");
var greenrange = document.getElementById("greenRange");
var bluerange = document.getElementById("blueRange");
var updateledcolor = document.getElementById("updateLedColor");
updateledcolor.addEventListener("click", function(){
	update_actuators();
    actuators[0] = "0";
	actuators[8] = redrange.value;
	actuators[9] = greenrange.value;
	actuators[10] = bluerange.value;
	actuators_string = actuators.join(); 
	console.log("UPDATE LED COLOR");
	send_string(actuators_string);
});
