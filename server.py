###############################################################################
###                         SMART GREENHOUSE SERVER                         ###
###                                                                         ###
### UNIVERSITY OF CAMPINAS (UNICAMP)                                        ###
### SCHOOL OF ELECTRICAL ENGINEERING AND COMPUTING                          ###
### IA351D - AUTOMATION PROJECT WITH RASPBERRY PI - 2016.2                  ###
### AUTORS: FELICIO HARLEY GARCIA DE CASTRO and JUAN PABLO PANTOJA BASTIDAS ###
### TEACHERS: FABIAO FRUETT and LUCAS HEITZMANN GABRIELLI                   ###
###############################################################################

###############################################################################
### Modules importing BEGIN ###

# Web Service modules
import flask
import flask_socketio
# DHT sensor module
import Adafruit_DHT
# PWM driver module
import Adafruit_PCA9685
# SPI library (for hardware SPI
import Adafruit_GPIO.SPI as SPI
# MCP3008 ADC module
import Adafruit_MCP3008
# GPIO module
import RPi.GPIO as GPIO
# General modules
import sys
import time
import threading

### Modules importing END ###

###############################################################################
### Pin assignment BEGIN ###

## Inputs ##
# DHT11 sensor pin (1)
dht11_sensor_pin = 25

## Outputs ##
# Step motor pins (4)
step_motor_pin = [16, 20, 21, 26]

# Heater lamp pin
heater_lamp_pin = 4

# Fans pins (4)
heater_fan_pin  = 24
cooler_fan_pin = 0
circulation_fan_pin = 23

# Pulverizer and irrigator pins (2)
pulverizer_pin = 22
irrigator_pin = 17

### Pin assignment END ###

###############################################################################
### GPIO configuration BEGIN ###

# GPIO numbering
GPIO.setmode(GPIO.BCM)
# GPIO outputs setup
outputs = [step_motor_pin[0], step_motor_pin[1], step_motor_pin[2],
           step_motor_pin[3], heater_lamp_pin, heater_fan_pin,
           cooler_fan_pin, circulation_fan_pin, pulverizer_pin, irrigator_pin]
for pin in outputs:
    GPIO.setup(pin,GPIO.OUT)
    GPIO.output(pin, False)

### GPIO configuration END ###

###############################################################################
### Object instantiation BEGIN ###

# SocketIO instantiation
app = flask.Flask(__name__)
app.config.update(
    DEBUG=False,
    SECRET_KEY='secret!',
    )

socketio = flask_socketio.SocketIO(app)
thread = None

# DHT11 sensor instantiation
dht11_sensor = Adafruit_DHT.DHT11

# PCA9685 PWM driver instantiation
pwm = Adafruit_PCA9685.PCA9685()
# Set frequency
pwm.set_pwm_freq(600)

doorLock = threading.Lock()

### Object instantiation END ###

###############################################################################
### General variables BEGIN ###

global previous_msg
global door_status
global preset_program_status
global red_led_status
global green_led_status
global blue_led_status
global amb_temp
global amb_humid
global soil_humidity_status
global water_level_status
global preset_thread
global preset

### General variables END ###

###############################################################################
### Function definitions BEGIN ###

def fetch_data():
    """Acquire and send sensor and actuators status to the client every second"""
    global door_status
    global preset_program_status
    global red_led_status
    global green_led_status
    global blue_led_status
    global amb_temp
    global amb_humid
    global soil_humidity_status
    global water_level_status
    while True:
        with open('/sys/class/thermal/thermal_zone0/temp', 'r') as fin:
            temp = int(fin.readline()) / 1000

        # Analog sensors
        read_analog_sensors()
        # Transforming color status to hexadecimal (HTML friendly)
        color_status = color_html(red_led_status, green_led_status, blue_led_status)
        
        # Checking output pins status
        i=0
        status = [None] * len(outputs)
        for out_pin in outputs:
            status[i] = GPIO.input(out_pin)
            i += 1

        # Sending the message to client

        # data0 = raspberry_temperature
        # data1 = amb_temp
        # data2 = amb_humid
        # data3 = soil_humidity_status
        # data4 = water_level_status
        # data5 = preset_program_status
        # data6 = door_status
        # data7 = heater_lamp_status
        # data8 = heater_fan_status
        # data9 = cooler_fan_status
        # data10 = circulation_fan_status
        # data11 = pulverizer_status
        # data12 = irrigator_status
        # data13 = red_intensity_status
        # data14 = green_intensity_status
        # data15 = blue_intensity_status

        msg = '{:.1f}'.format(temp)+','+str(amb_temp)+','+str(amb_humid)+','+soil_humidity_status+','+water_level_status+','+str(preset_program_status)+','+str(door_status)+','+','.join(str(e) for e in status[4:])+','+str(red_led_status)+','+str(green_led_status)+','+str(blue_led_status)+','+color_status
        socketio.send(msg)
        print('message sent1: ', msg)
        socketio.sleep(1)
		
def door_actuate(status):
    """Open and close the doors using the 28BYJ-48 stepper motor"""
    global door_status
    revolutions = 0.8
    cycles = int(revolutions * 512)

    # Activation sequence for clockwise movement
    seq_cw = [[1,0,0,0],
              [1,1,0,0],
              [0,1,0,0],
              [0,1,1,0],
              [0,0,1,0],
              [0,0,1,1],
              [0,0,0,1],
              [1,0,0,1]]
    # Activation sequence for counterclockwise movement
    seq_ccw = [[0,0,0,1],
               [0,0,1,1],
               [0,0,1,0],
               [0,1,1,0],
               [0,1,0,0],
               [1,1,0,0],
               [1,0,0,0],
               [1,0,0,1]]
	
    # Change pins status using the appropiate stepping sequence open or close the doors	
	
    if (status == 0 and door_status == 1):
        door_status = 0
        doorLock.acquire()
        for i in range(cycles):
            for halfstep in range(8):
                for pin in range(4):
                    GPIO.output(step_motor_pin[pin], seq_ccw[halfstep][pin])
                time.sleep(0.001)
        doorLock.release()

    if (status == 1 and door_status == 0):
        door_status = 1
        doorLock.acquire()
        for i in range(cycles):
            for halfstep in range(8):
                for pin in range(4):
                    GPIO.output(step_motor_pin[pin], seq_cw[halfstep][pin])
                time.sleep(0.001)
        doorLock.release()		
				
    for pin in step_motor_pin:
        GPIO.output(pin, False)

def dht11_sensor_read():
    """<<THREAD>> Reads DHT11 sensor data every 2 seconds and updates a global variable"""
    global amb_temp
    global amb_humid
    while True:
        doorLock.acquire()
        doorLock.release()
        humidity, temperature = Adafruit_DHT.read_retry(dht11_sensor, dht11_sensor_pin);
        if humidity is not None and temperature is not None and temperature > 15 and humidity <= 100:
            amb_temp = int(temperature)
            amb_humid = int(humidity)
        else:
        # Sensor communication error message
            print("Fail to read DHT11 data")
        time.sleep(2)		

def read_analog_sensors():
    """Reads analog sensor data using the ADC MCP3008 and updates global variables"""
    global soil_humidity_status
    global water_level_status
    # Hardware SPI configuration:
    SPI_PORT   = 0
    SPI_DEVICE = 0
    mcp = Adafruit_MCP3008.MCP3008(spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))

    # Soil humidity sensor reading
    soil_humidity_reading = mcp.read_adc(0)   
    soil_humidity_pct = int((1023 - soil_humidity_reading) * 100.0/623)
    soil_humidity_status = str(soil_humidity_pct)+"%"

    # Water level sensor reading
    water_level_reading = mcp.read_adc(1)
    if water_level_reading >= 850 and water_level_reading <= 1023:
	    water_level_status = "High"
    elif water_level_reading >= 450 and water_level_reading < 850:
	    water_level_status = "Medium"
    else:
        water_level_status = "Low"

def led_rgb_color(red, green, blue):
    """Control the color of the RGB LED strip using a PCA9685 PWM Driver"""
    global red_led_status
    global green_led_status
    global blue_led_status

    pwm.set_pwm(0, 0, red)
    red_led_status = red
    pwm.set_pwm(7, 0, green)
    green_led_status = green
    pwm.set_pwm(15, 0, blue)
    blue_led_status = blue

def led_control_demonstration():
    """Performs a RGB LED strip demonstration"""
    led_rgb_color(4095, 4095, 4095)
    time.sleep(3)
    led_rgb_color(0, 0, 0)
    time.sleep(0.2)
    led_rgb_color(4095, 0, 0)
    time.sleep(2.5)
    led_rgb_color(0, 0, 0)
    time.sleep(0.2)
    led_rgb_color(0, 4095, 0)
    time.sleep(2.5)
    led_rgb_color(0, 0, 0)
    time.sleep(0.2)
    led_rgb_color(0, 0, 4095)
    time.sleep(2.5)
    led_rgb_color(0, 0, 0)

    for i in range(0,4096,5):
        led_rgb_color(i, 0, 0)
    for j in range(0,4096,5):
        led_rgb_color(i, j, 0)   
    for k in range(0,4096,5):
        led_rgb_color(i, j, k)
    for i in range(4095,-1,-5):
        led_rgb_color(i, j, k)
    for j in range(4095,-1,-5):
        led_rgb_color(i, j, k)
    for k in range(4095,-1,-5):
        led_rgb_color(i, j, k)

def hex_string(n):
    """Transforms a decimal number into a hexadecimal string"""
        x =(n%16)
        if (x < 10):
            return str(x) 
        if (x == 10):
            return "A"
        if (x == 11):
            return "B"
        if (x == 12):
            return "C"
        if (x == 13):
            return "D"
        if (x == 14):
            return "E"
        if (x == 15):
            return "F"

def color_html(red_led_status, green_led_status, blue_led_status):
    """Transforms color status to hexadecimal (HTML friendly)"""
    red_scale = (red_led_status)//16
    red_mod = (red_led_status)%16
    red_color_html = hex_string((red_scale // 16))
    red_gamma_html = hex_string((red_mod))
    
    green_scale = (green_led_status)//16
    green_mod = (green_led_status)%16
    green_color_html = hex_string((green_scale // 16))
    green_gamma_html = hex_string((green_mod))
    
    blue_scale = (blue_led_status)//16
    blue_mod = (blue_led_status)%16
    blue_color_html = hex_string((blue_scale // 16))
    blue_gamma_html = hex_string((blue_mod))
    
    return "#" + red_color_html + red_gamma_html + green_color_html + green_gamma_html + blue_color_html + blue_gamma_html

def string_to_list(string):
    """Transforms a string into a list vector"""
    return list(map(int, string.split(',')))

def list_to_string(list):
    """Transforms a list vector into a string"""
    string = ','.join(str(e) for e in list)
    return string

def list_diff(list1, list2):
    """Checks the differences between lists and returns the index in which differences were found"""
    i = 0
    j = 0
    lenght = len(list1)
    diff_index = [None] * lenght
    for val in list1:
        if val != list2[i]:
            diff_index[j] = i
            j += 1
        i += 1
    k=0
    for k in range(0,lenght-j):
        diff_index.pop()
    return diff_index

def sleep(seconds):
    """Performs a pause for the specified time"""
    a=time.time()
    b=time.time()
    while(b-a < (0.1 * 10 * seconds)):
        b=time.time()

def humidity_demonstration():
    """Performs a demonstration of the pulverizer and irrigator systems"""
    for i in range(3):
        GPIO.output(irrigator_pin, 1)
        sleep(1)
        GPIO.output(irrigator_pin, 0)
        GPIO.output(pulverizer_pin, 1)
        sleep(3)
        GPIO.output(pulverizer_pin, 0)
    
def preset_program():
    """Performs a demonstration using all the actuators"""
    global preset_thread
    preset_thread = 1
    # Reset system
    led_rgb_color(0, 0, 0)
    for pin in outputs:
        GPIO.output(pin, False)
    door_actuate(0)

    # Start demonstration
    led_control_demonstration()
    sleep(0.2)
    led_rgb_color(4095, 4095, 4095)
    sleep(0.5)
    humidity_demonstration()
    #time.sleep(0.5)
    door_actuate(1)
    GPIO.output(heater_lamp_pin, 1)
    sleep(0.5)
    GPIO.output(heater_fan_pin, 1)
    sleep(0.5)
    GPIO.output(circulation_fan_pin, 1)
    sleep(20)
    GPIO.output(heater_lamp_pin, 0)
    sleep(0.5)
    GPIO.output(cooler_fan_pin, 1)
    sleep(20)
    GPIO.output(heater_fan_pin, 0)
    sleep(0.5)
    GPIO.output(cooler_fan_pin, 0)
    sleep(0.5)
    door_actuate(0)
    sleep(0.5)
    GPIO.output(circulation_fan_pin, 0)
    sleep(0.5)
    led_rgb_color(0, 0, 0)
    preset_thread = 0

### Function definitions END ###

###############################################################################
### System initialization BEGIN ###

previous_msg = "0,0,0,0,0,0,0,0,0,0,0"
door_status = 0
preset_program_status = 0
red_led_status = 0
green_led_status = 0
blue_led_status = 0
led_rgb_color(red_led_status, green_led_status, blue_led_status)
amb_temp = 0
amb_humid = 0
water_level_status = "-"
soil_humidity_status = "-"
preset_thread = 0

### System initialization END ###

###############################################################################
### Web communication functions BEGIN ###

@app.route('/')
def index():
    return flask.render_template('index.html')

@socketio.on('connect')
def open_conection():
    global thread
    if thread is None:
        thread = socketio.start_background_task(target=fetch_data)
    print('connection opened')

@socketio.on('disconnect')
def close_conection():
    print('connection closed')

@socketio.on('message')
def receive_message(msg):
    """Receives the message from the client and execute commands on actuators"""
    global previous_msg
    global door_status
    global preset_program_status
    global preset_thread
    global preset
    print('message received: ', msg)
    if msg != previous_msg:
        print(previous_msg)
        print(msg)
        previous_msg = string_to_list(previous_msg)
        updated_msg = string_to_list(msg)
        dif = list_diff(updated_msg, previous_msg)
        print("Changes index:", dif)
        previous_msg = msg
        for change in dif:
            if change == 0:
                preset_program_status = updated_msg[0]
                if updated_msg[0] != 0:
                    if preset_thread == 0:
                        preset.start()
                print("preset_program(" + str(updated_msg[0]) + ")")
            elif change == 1:
                door_actuate(updated_msg[1])
            elif change == 8 or change == 9 or change == 10:
                led_rgb_color(updated_msg[8], updated_msg[9], updated_msg[10])
                print("led_rgb_color(" + str(updated_msg[8]) + "," + str(updated_msg[9]) + "," + str(updated_msg[10]) + ")")
                return
            else:
                GPIO.output(outputs[change+2],updated_msg[change])
                print ("GPIO.output(" + str(outputs[change+2]) + "," + str(updated_msg[change]) + ")")
        
    else:
        print("Nothing changed")

### Web communication functions END ###

###############################################################################
### Server execution ###

def server_run():
    socketio.run(app, host='0.0.0.0')

def Main():
    global preset
    t1 = threading.Thread(target=server_run)
    t2 = threading.Thread(target=dht11_sensor_read)
    preset = threading.Thread(target=preset_program)
    t1.start()
    t2.start()

if __name__ == '__main__':
    Main()

	
