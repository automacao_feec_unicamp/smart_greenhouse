# Smart Greenhouse

![Video](https://gitlab.com/automacao_feec_unicamp/smart_greenhouse/wikis/blob/video.mp4)

Sistema de automação para estufas usando o Raspberry Pi.

Informações: [wiki](https://gitlab.com/automacao_feec_unicamp/smart_greenhouse/wikis/home)

## Autores

[Felício Harley Garcia de Castro](https://gitlab.com/felicioharley), Juan Pablo Pantoja Bastidas
